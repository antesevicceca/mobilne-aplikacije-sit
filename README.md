# Mobilne aplikacije SIT (2022/2023)


Materijali za vežbe na predmetu **Mobilne aplikacije (MA) 2022/2023** na smeru **OSS Softversko i informacione tehnologije (SIT)**, Fakultet tehničkih nauka, Novi Sad. 

[![Ftn logo](ftn-logo.png)](http://www.ftn.uns.ac.rs/691618389/fakultet-tehnickih-nauka)

